package com.example.spring_app.sample3;

public class RequestMessage {
    private String requestBody;


    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }
}
