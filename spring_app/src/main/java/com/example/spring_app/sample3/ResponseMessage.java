package com.example.spring_app.sample3;

public class ResponseMessage {
    private String responseBody;

    public ResponseMessage(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }
}
