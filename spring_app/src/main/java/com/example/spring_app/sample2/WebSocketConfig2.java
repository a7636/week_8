package com.example.spring_app.sample2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig2  implements WebSocketConfigurer {
    @Autowired
    private HttpAuthHandler httpAuthHandler;
    @Autowired
    private MyInterceptor myInterceptor;


    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry
                .addHandler(httpAuthHandler, "/myWS2")
                .addInterceptors(myInterceptor)
                .setAllowedOrigins("*");
    }

    @Bean
    public WebSocketConnectionManager wsConnectionManager() {

        //Generates a web socket connection
        String webSocketUri = "ws://localhost:8025/websockets/game";
        WebSocketConnectionManager manager = new WebSocketConnectionManager(
                new StandardWebSocketClient(),
                new ClientSocketHandler(), //Must be defined to handle messages
                webSocketUri);

        //Will connect as soon as possible
        manager.setAutoStartup(true);

        return manager;
    }
}
