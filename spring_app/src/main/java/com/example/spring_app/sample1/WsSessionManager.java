package com.example.spring_app.sample1;



import javax.websocket.Session;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

public class WsSessionManager {
    /**
     * Where to save the connection session
     */
    private static ConcurrentHashMap<String, Session> SESSION_POOL = new ConcurrentHashMap<>();

    /**
     * Add session
     *
     * @param key
     */
    public static void add(String key, Session session) {
        // Add session
        SESSION_POOL.put(key, session);
    }

    /**
     * Deleting a session will return the deleted session
     *
     * @param key
     * @return
     */
    public static Session remove(String key) {
        // Delete session
        return SESSION_POOL.remove(key);
    }

    /**
     * Delete and sync close connection
     *
     * @param key
     */
    public static void removeAndClose(String key) {
        Session session = remove(key);
        if (session != null) {
            try {
                // Close connection
                session.close();
            } catch (IOException e) {
                // todo: exception handling during shutdown
                e.printStackTrace();
            }
        }
    }

    /**
     * Get session
     *
     * @param key
     * @return
     */
    public static Session get(String key) {
        // Get session
        return SESSION_POOL.get(key);
    }
}