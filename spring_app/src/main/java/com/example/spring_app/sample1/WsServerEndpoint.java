package com.example.spring_app.sample1;

import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
@Component
@ServerEndpoint("/myWs")
public class WsServerEndpoint {

    /**
     * Successful connection
     *
     * @param session
     */
    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Successful connection");
    }

    /**
     * Connection closure
     *
     * @param session
     */
    @OnClose
    public void onClose(Session session) {
        System.out.println("Connection closure");
    }

    /**
     * Message received
     *
     * @param text
     */
    @OnMessage
    public String onMsg(String text) throws IOException {
        WsSessionManager.get("0").getBasicRemote().sendText(text);
        return "servet Send out:" + text;
    }
}
