package com.example.spring_app.sample1;

import org.springframework.stereotype.Component;

import javax.websocket.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Logger;

@Component
@ClientEndpoint
public class SampleClientEndpoint {



    @OnOpen

    public void onOpen(Session session) {
        WsSessionManager.add("0", session);

        System.out.println("Connected qstr... " + session.getQueryString());

        try {


            session.getBasicRemote().sendText("start");

        } catch (IOException e) {

            throw new RuntimeException(e);

        }

    }



    @OnMessage

    public void onMessage(String message, Session session) {

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        try {

            System.out.println("Received ...." + message);

            String userInput = bufferRead.readLine();
            session.getBasicRemote().sendText("to the session");
//            return userInput;

        } catch (IOException e) {

            throw new RuntimeException(e);

        }

    }



    @OnClose

    public void onClose(Session session, CloseReason closeReason) {

        System.out.println(String.format("Session %s close because of %s", session.getId(), closeReason));

    }

  
}
