package com.example.spring_app.sample1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.client.standard.AnnotatedEndpointConnectionManager;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

@Configuration
@EnableWebSocket
public class WebSocketConfig {
    @Autowired
    SampleClientEndpoint clientEndpoint;

    @Bean
    public ServerEndpointExporter serverEndpoint() {
        return new ServerEndpointExporter();
    }

    @Bean
    public AnnotatedEndpointConnectionManager connectionManager() {
        AnnotatedEndpointConnectionManager annotatedEndpointConnectionManager=new AnnotatedEndpointConnectionManager(clientEndpoint, "ws://localhost:8025/websockets/game");
        annotatedEndpointConnectionManager.start();
        return annotatedEndpointConnectionManager;
    }

}