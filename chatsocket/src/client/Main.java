package client;

public class Main {
    public static void main(String[] args) {

        int port = 5000;

        ChatClient client = new ChatClient("localhost", port);
        client.execute();
    }
}
