package server;

import java.io.*;
import java.net.Socket;

public class UserThread extends Thread {
    private Socket socket;
    private ChatServer chatServer;
    private PrintWriter writer;
    private BufferedReader reader;
    private String clientName;

    public String getClientName() {
        return clientName;
    }

    UserThread(Socket socket, ChatServer chatServer) {
        this.socket = socket;
        this.chatServer = chatServer;
    }

    @Override
    public void run() {
        try {
            InputStream input = socket.getInputStream();
            reader = new BufferedReader(new InputStreamReader(input));


            OutputStream output = socket.getOutputStream();

            writer = new PrintWriter(output, true);
            writer.println("Please type unique name!");

            String name = reader.readLine();
            while (this.chatServer.containsUser(name)) {
                writer.println("This name is exists, please type another name ");
                name = reader.readLine();
            }
            ;
            this.clientName = name;
            chatServer.addUserThread(name, this);

            sendMessage("Users: ");
            for (String clientName : chatServer.getUserThreadMap().keySet()) {
                sendMessage(clientName);
            }

            String msg;
            do {
                msg = reader.readLine();
                if (msg.equals("bye")) {
                    break;
                } else if (msg.contains(":")) {
                    String to = msg.split(":")[0];//admin:hello
                    msg = msg.split(":")[1];
                    chatServer.sendMessage(msg, this.clientName, to);

                } else {
                    chatServer.broadcast(msg, getClientName());
                }
            } while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        writer.println(message);
    }

}
